// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKart/GoKartMovementReplicator.h"

#include "GoKart/GoKartMovementComponent.h"
#include "Net/UnrealNetwork.h"

UGoKartMovementReplicator::UGoKartMovementReplicator()
{
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
}

void UGoKartMovementReplicator::BeginPlay()
{
	Super::BeginPlay();

	MovementComponent = GetOwner()->FindComponentByClass<UGoKartMovementComponent>();
}

void UGoKartMovementReplicator::OnRep_ServerState()
{
	if (!MovementComponent) return;

	switch (GetOwnerRole())
	{
	case ROLE_AutonomousProxy:
		AutonomousProxy_OnRep_ServerState();
		break;
	case ROLE_SimulatedProxy:
		SimulatedProxy_OnRep_ServerState();
		break;
	default: ;
	}
}

void UGoKartMovementReplicator::SimulatedProxy_OnRep_ServerState()
{
	Client_TimeBetweenLastUpdates = Client_TimeSinceUpdate;
	Client_TimeSinceUpdate = 0;

	if (MeshOffsetRoot)
		Client_StartTransform = FTransform(MeshOffsetRoot->GetComponentQuat(), MeshOffsetRoot->GetComponentLocation());

	Client_StartVelocity = MovementComponent->GetVelocity();

	GetOwner()->SetActorTransform(ServerState.Transform);
}

void UGoKartMovementReplicator::AutonomousProxy_OnRep_ServerState()
{
	GetOwner()->SetActorTransform(ServerState.Transform);
	if (MovementComponent)
		MovementComponent->SetVelocity(ServerState.Velocity);

	ClearAcknowledgeMoves(ServerState.LastMove);

	for (const FGoKartMove& Move : UnacknowledgedMoves)
		if (MovementComponent)
			MovementComponent->SimulateMove(Move);
}

void UGoKartMovementReplicator::UpdateServerState(const FGoKartMove& Move)
{
	ServerState.LastMove = Move;
	ServerState.Transform = GetOwner()->GetActorTransform();
	ServerState.Velocity = MovementComponent->GetVelocity();
}

void UGoKartMovementReplicator::ClientTick(float DeltaTime)
{
	Client_TimeSinceUpdate += DeltaTime;

	if (Client_TimeBetweenLastUpdates < KINDA_SMALL_NUMBER) return;

	const float LerpRatio = Client_TimeSinceUpdate / Client_TimeBetweenLastUpdates;
	const FHermiteCubicSpline Spline(
		Client_StartTransform.GetLocation(),
		Client_StartVelocity * VelocityToDerivative(),
		ServerState.Transform.GetLocation(),
		ServerState.Velocity * VelocityToDerivative()
	);

	InterpolateLocation(Spline, LerpRatio);
	InterpolateVelocity(Spline, LerpRatio);
	InterpolateRotation(LerpRatio);
}

float UGoKartMovementReplicator::VelocityToDerivative() const
{
	return Client_TimeBetweenLastUpdates * 100.0f;
}

void UGoKartMovementReplicator::InterpolateLocation(const FHermiteCubicSpline& Spline, float LerpRatio) const
{
	const FVector NewLocation = Spline.InterpolateLocation(LerpRatio);

	if (MeshOffsetRoot)
		MeshOffsetRoot->SetWorldLocation(NewLocation);
}

void UGoKartMovementReplicator::InterpolateVelocity(const FHermiteCubicSpline& Spline, float LerpRatio) const
{
	const FVector NewDerivative = Spline.InterpolateDerivative(LerpRatio);
	const FVector NewVelocity = NewDerivative / VelocityToDerivative();

	MovementComponent->SetVelocity(NewVelocity);
}

void UGoKartMovementReplicator::InterpolateRotation(float LerpRatio) const
{
	const FQuat TargetRotation = ServerState.Transform.GetRotation();
	const FQuat StartRotation = Client_StartTransform.GetRotation();

	const FQuat NewRotation = FQuat::Slerp(StartRotation, TargetRotation, LerpRatio);

	if (MeshOffsetRoot)
		MeshOffsetRoot->SetWorldRotation(NewRotation);
}

void UGoKartMovementReplicator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!MovementComponent) return;

	const FGoKartMove LastMove = MovementComponent->GetLastMove();

	// We are the client
	if (GetOwnerRole() == ROLE_AutonomousProxy)
	{
		UnacknowledgedMoves.Add(LastMove);
		Server_SendMove(LastMove);
	}

	// We are the server and in control of the pawn
	if (GetOwner()->HasAuthority() && Cast<APawn>(GetOwner())->IsLocallyControlled())
	{
		UpdateServerState(LastMove);
	}

	if (GetOwnerRole() == ROLE_SimulatedProxy)
		ClientTick(DeltaTime);
}

void UGoKartMovementReplicator::ClearAcknowledgeMoves(const FGoKartMove& LastMove)
{
	TArray<FGoKartMove> NewMoves;

	for (const FGoKartMove& Move : UnacknowledgedMoves)
		if (Move.Time > LastMove.Time)
			NewMoves.Add(Move);

	UnacknowledgedMoves = NewMoves;
}

void UGoKartMovementReplicator::Server_SendMove_Implementation(const FGoKartMove& Move)
{
	if (MovementComponent)
		MovementComponent->SimulateMove(Move);

	Client_SimulatedTime += Move.DeltaTime;
	UpdateServerState(Move);
}

bool UGoKartMovementReplicator::Server_SendMove_Validate(const FGoKartMove& Move)
{
	const float ProposedTime = Client_SimulatedTime + Move.DeltaTime;
	const bool ClientNotRunningAhead = ProposedTime < GetWorld()->TimeSeconds;
	if (!ClientNotRunningAhead)
	{
		UE_LOG(LogTemp, Error, TEXT("Client is running too fast!"));
		return false;
	}
	if (!Move.IsValid())
	{
		UE_LOG(LogTemp, Error, TEXT("Recieved invalid move!"));
		return false;
	}

	return true;
}

void UGoKartMovementReplicator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UGoKartMovementReplicator, ServerState);
}
