// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKart/GoKart.h"

#include "DrawDebugHelpers.h"
#include "GoKart/GoKartMovementComponent.h"
#include "GoKart/GoKartMovementReplicator.h"

AGoKart::AGoKart()
{
	PrimaryActorTick.bCanEverTick = true;

	MovementComponent = CreateDefaultSubobject<UGoKartMovementComponent>("MovementComponent");
	MovementReplicator = CreateDefaultSubobject<UGoKartMovementReplicator>("MovementReplicator");

	SetReplicates(true);
	SetReplicateMovement(false);
}

void AGoKart::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		NetUpdateFrequency = 1.0f;
		// MinNetUpdateFrequency = 100.0f;
	}
}

FString GetEnumText(ENetRole Role)
{
	switch (Role)
	{
	case ROLE_None:
		return "None";
	case ROLE_SimulatedProxy:
		return "SimulatedProxy";
	case ROLE_AutonomousProxy:
		return "AutonomousProxy";
	case ROLE_Authority:
		return "Authority";
	case ROLE_MAX:
		return "MAX";
	default:
		return "ERROR!";
	}
}

void AGoKart::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DrawDebugString(GetWorld(), FVector(0, 0, 100), GetEnumText(GetLocalRole()), this,
	                FColor::Red, DeltaTime);
}

void AGoKart::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AGoKart::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGoKart::MoveRight);
}

void AGoKart::MoveForward(float Value)
{
	MovementComponent->SetThrottle(Value);
}

void AGoKart::MoveRight(float Value)
{
	MovementComponent->SetSteeringThrow(Value);
}
