// Fill out your copyright notice in the Description page of Project Settings.

#include "GoKart/GoKartMovementComponent.h"

UGoKartMovementComponent::UGoKartMovementComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	Mass = 1000.0f;
	MaxDrivingForce = 10000.0f;
	MinimumTurningRadius = 10.0f;
	DragCoefficient = 16.0f; // Cause 25 m/s of high speed
	RollingResistanceCoefficient = 0.05f;
}

void UGoKartMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UGoKartMovementComponent::UpdateLocationFromVelocity(float DeltaTime)
{
	const FVector Translation = Velocity * DeltaTime * 100;

	FHitResult HitResult;
	GetOwner()->AddActorWorldOffset(Translation, true, &HitResult);

	if (HitResult.IsValidBlockingHit())
		Velocity = FVector::ZeroVector;
}

#pragma warning (disable : 4458)
void UGoKartMovementComponent::ApplyRotation(float DeltaTime, float SteeringThrow)
{
	const float DeltaLocation = FVector::DotProduct(GetOwner()->GetActorForwardVector(), Velocity) * DeltaTime;
	const float RotationAngle = DeltaLocation / MinimumTurningRadius * SteeringThrow;
	const FQuat RotationDelta(GetOwner()->GetActorUpVector(), RotationAngle);
	Velocity = RotationDelta.RotateVector(Velocity);
	GetOwner()->AddActorWorldRotation(RotationDelta, true);
}
#pragma warning (default : 4458)

FVector UGoKartMovementComponent::CalculateAirResistance() const
{
	const FVector Direction = -1.0f * Velocity.GetSafeNormal();
	return Velocity.SizeSquared() * DragCoefficient * Direction;
}

FVector UGoKartMovementComponent::CalculateRollingResistance() const
{
	const FVector Direction = -1.0f * Velocity.GetSafeNormal();
	const float G = -1.0f * GetWorld()->GetGravityZ() / 100;
	const float NormalForce = Mass * G;
	return NormalForce * RollingResistanceCoefficient * Direction;
}

FGoKartMove UGoKartMovementComponent::CreateMove(const float& DeltaTime) const
{
	return FGoKartMove(Throttle, SteeringThrow, DeltaTime, GetWorld()->TimeSeconds);
}

void UGoKartMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetOwnerRole() == ROLE_AutonomousProxy || GetOwner()->HasAuthority() && Cast<APawn>(GetOwner())->IsLocallyControlled())
	{
		LastMove = CreateMove(DeltaTime);

		SimulateMove(LastMove);
	}
}

void UGoKartMovementComponent::SimulateMove(const FGoKartMove& Move)
{
	FVector Force = GetOwner()->GetActorForwardVector() * MaxDrivingForce * Move.Throttle;

	Force += CalculateAirResistance();
	Force += CalculateRollingResistance();

	const FVector Acceleration = Force / Mass;

	Velocity += Acceleration * Move.DeltaTime;

	UpdateLocationFromVelocity(Move.DeltaTime);

	ApplyRotation(Move.DeltaTime, Move.SteeringThrow);
}
