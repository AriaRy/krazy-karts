// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/ActorComponent.h"
#include "GoKartMovementReplicator.generated.h"

class UGoKartMovementComponent;
USTRUCT()
struct FGoKartMove
{
	GENERATED_BODY()

	FGoKartMove(const float Throttle = 0.0f,
	            const float SteeringThrow = 0.0f,
	            const float DeltaTime = 0.0f,
	            const float Time = 0.0f) :
		Throttle(Throttle),
		SteeringThrow(SteeringThrow),
		DeltaTime(DeltaTime),
		Time(Time)
	{
	}

	UPROPERTY()
	float Throttle;

	UPROPERTY()
	float SteeringThrow;

	UPROPERTY()
	float DeltaTime;

	UPROPERTY()
	float Time;

	bool IsValid() const
	{
		return FMath::Abs(Throttle) <= 1 && FMath::Abs(SteeringThrow) <= 1;
	}
};

USTRUCT()
struct FGoKartState
{
	GENERATED_BODY()

	UPROPERTY()
	FGoKartMove LastMove;

	UPROPERTY()
	FVector Velocity;

	UPROPERTY()
	FTransform Transform;
};

struct FHermiteCubicSpline
{
	FHermiteCubicSpline(const FVector& StartLocation = FVector::ZeroVector, const FVector& StartDerivative = FVector::ZeroVector,
	                    const FVector& TargetLocation = FVector::ZeroVector, const FVector& TargetDerivative = FVector::ZeroVector) :
		StartLocation(StartLocation), StartDerivative(StartDerivative),
		TargetLocation(TargetLocation), TargetDerivative(TargetDerivative)
	{
	}

	FVector StartLocation, StartDerivative,	TargetLocation, TargetDerivative;

	FVector InterpolateLocation(const float LerpRatio) const
	{
		return FMath::CubicInterp(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRatio);
	}

	FVector InterpolateDerivative(const float LerpRatio) const
	{
		return FMath::CubicInterpDerivative(StartLocation, StartDerivative, TargetLocation, TargetDerivative, LerpRatio);
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KRAZYKARTS_API UGoKartMovementReplicator : public UActorComponent
{
	GENERATED_BODY()

public:

	UGoKartMovementReplicator();

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnRep_ServerState();

	UFUNCTION()
	void SimulatedProxy_OnRep_ServerState();

	UFUNCTION()
	void AutonomousProxy_OnRep_ServerState();

	void UpdateServerState(const FGoKartMove& Move);

	void ClientTick(float DeltaTime);

	float VelocityToDerivative() const;

	void InterpolateLocation(const FHermiteCubicSpline& Spline, float LerpRatio) const;

	void InterpolateVelocity(const FHermiteCubicSpline& Spline, float LerpRatio) const;

	void InterpolateRotation(float LerpRatio) const;

	UFUNCTION(BlueprintCallable, Category = "Replication", meta = (Tooltip = "Sets the MeshOffsetRoot for the c++ class"))
	void SetMeshOffsetRoot(USceneComponent* Root) { MeshOffsetRoot = Root; }

public:

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void ClearAcknowledgeMoves(const FGoKartMove& LastMove);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SendMove(const FGoKartMove& Move);

protected:

	UPROPERTY()
	UGoKartMovementComponent* MovementComponent;

	UPROPERTY(ReplicatedUsing = OnRep_ServerState)
	FGoKartState ServerState;

	TArray<FGoKartMove> UnacknowledgedMoves;

	float Client_TimeSinceUpdate;

	float Client_TimeBetweenLastUpdates;

	float Client_SimulatedTime;

	FTransform Client_StartTransform;

	FVector Client_StartVelocity;

	UPROPERTY()
	USceneComponent* MeshOffsetRoot;
};
