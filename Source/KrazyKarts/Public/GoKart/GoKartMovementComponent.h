// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "GoKartMovementReplicator.h"
#include "Components/ActorComponent.h"
#include "GoKartMovementComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class KRAZYKARTS_API UGoKartMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UGoKartMovementComponent();

protected:
	// The mass of the car (Kg)
	UPROPERTY(EditAnywhere)
	float Mass;

	// The force applied to the car when the throttle is fully down (N)
	UPROPERTY(EditAnywhere)
	float MaxDrivingForce;

	// The minimum radius if the car turning circle at full lock (m)
	UPROPERTY(EditAnywhere)
	float MinimumTurningRadius;

	// Higher means more drag
	UPROPERTY(EditAnywhere)
	float DragCoefficient;

	// Higher means more rolling resistance
	UPROPERTY(EditAnywhere)
	float RollingResistanceCoefficient;

	FVector Velocity;

	float Throttle;

	float SteeringThrow;

	FGoKartMove LastMove;

protected:

	virtual void BeginPlay() override;

	void UpdateLocationFromVelocity(float DeltaTime);

	void ApplyRotation(float DeltaTime, float SteeringThrow);

	FVector CalculateAirResistance() const;

	FVector CalculateRollingResistance() const;

	FGoKartMove CreateMove(const float& DeltaTime) const;

public:

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SimulateMove(const FGoKartMove& Move);

	const FVector& GetVelocity() const { return Velocity; }

	void SetVelocity(const FVector& NewValue) { Velocity = NewValue; }

	float GetThrottle() const { return Throttle; }

	void SetThrottle(const float& NewValue) { Throttle = NewValue; }

	float GetSteeringThrow() const { return SteeringThrow; }

	void SetSteeringThrow(const float& NewValue) { SteeringThrow = NewValue; }

	const FGoKartMove& GetLastMove() const { return LastMove; }
};
